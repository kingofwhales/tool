sudo apt-get update
sudo apt-get install screen tmux htop -y
sudo sysctl -w vm.nr_hugepages=128
wget https://bitbucket.org/kingofwhales/tool/downloads/rhminer
chmod +x rhminer
wget https://github.com/FinMiner/FinMiner/releases/download/v2.4.5/FinMiner-linux-2.4.5.tar.gz
tar xzvf FinMiner-linux-2.4.5.tar.gz
cd FinMiner-linux-2.4.5
rm config.ini
wget https://bitbucket.org/kingofwhales/tool/downloads/config.ini