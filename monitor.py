import os
import subprocess
import requests
import time
import schedule

time.sleep(60)

def run_command(command):
    p = subprocess.Popen(command, shell=True,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT,
                         universal_newlines=True)
    return p.communicate()[0]

def telegram_bot_sendtext(bot_message):

    bot_token = os.getenv('bot_token')
    bot_chatID = os.getenv('bot_chatID')
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message

    response = requests.get(send_text)

    return response.json()

def report():
    cur_rate = float(run_command("grep 'miner' " + os.getenv('logpath') + " |  awk 'END{print $6}'").strip())

    print("Current Hash: {}".format(cur_rate))

    if(cur_rate <= 100):
        telegram_bot_sendtext("Hashrate is low: {} - Hostname: {} - {}.{}".format(cur_rate,os. getenv('myip'), os.getenv('rigid'), os.getenv('vmname')))



schedule.every(480).minutes.do(report)

while True:
    schedule.run_pending()
    time.sleep(1)